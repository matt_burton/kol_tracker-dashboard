import React, { Component } from "react";
import { connect } from "react-redux";
import KolDeck from "./page_components/kolcard";
import { filter } from "lodash";
import { Route, Switch, Redirect } from "react-router-dom";
import routes from "routes.js";
import { Row } from "reactstrap";
// core components
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import Footer from "components/Footer/Footer.js";
import Sidebar from "components/Sidebar/Sidebar.js";
class PostsIndex extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: "blue",
      sidebarOpened:
        document.documentElement.className.indexOf("nav-open") !== -1
    };
  }
  componentWillMount() {}

  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  testing(val, id) {
    var imageProfile = filter(this.props.images, function(o) {
      if (o.sys.id == val.fields.profile.sys.id) return o;
    });
    return <KolDeck kolDetails={val} allImages={imageProfile} key={id} />;
  }

  render() {
    // console.log(this.props);
    return (
      <>
        <div className="wrapper">
          <Sidebar
            {...this.props}
            routes={routes}
            // logo={{
            //   outterLink: "https://www.creative-tim.com/",
            //   text: "Creative Tim",
            //   imgSrc: logo
            // }}
            toggleSidebar={this.toggleSidebar}
          />
          <div className="main-panel" ref="mainPanel">
            <AdminNavbar
              {...this.props}
              brandText={this.getBrandText(this.props.location.pathname)}
              toggleSidebar={this.toggleSidebar}
              sidebarOpened={this.state.sidebarOpened}
            />
            <Switch>
              {this.getRoutes(routes)}
              <div className="content">
                <Row>
                  <h2>Users overview</h2>
                </Row>
                <Row>
                  {this.props.kols.map((val, inx) => {
                    return this.testing(val, inx);
                  })}
                </Row>
              </div>
            </Switch>
            {// we don't want the Footer to be rendered on map page
            this.props.location.pathname.indexOf("maps") !== -1 ? null : (
              <Footer fluid />
            )}
          </div>
        </div>
      </>
    );
  }
}
function mapStateToProps(state) {
  return { kols: state.kols.all, images: state.kols.images };
}
export default connect(mapStateToProps, {})(PostsIndex);
