import React from "react";
import { useAuth0 } from "../../react-auth0-spa";

const LoginOut = () => {
  const { isAuthenticated, loginWithRedirect, logout, redirect } = useAuth0();

  return (
    <div>
      {!isAuthenticated && (
        <button onClick={() => loginWithRedirect({})}>Log in</button>
      )}

      {isAuthenticated && <button onClick={() => logout()}>Log out</button>}
    </div>
  );
};

export default LoginOut;
