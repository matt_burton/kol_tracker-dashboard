import React, { useEffect } from 'react'
import GenHook from '../../hooks/kol_hooks'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
 

const KolDetailsCard = ({kolDetails,allImages, kolsEngagements}) => {
    const { value, setStateValue } = GenHook('');//Get Form Hook functions from input hook
    if(allImages[0]){
        if(!value.imageUrl){
            setStateValue('imageUrl', allImages[0].fields.file.url+'?w=50')
        }
    }

    let returnItem = []; let docRender;
    for (let [key, value] of Object.entries(kolDetails.fields)) {
       
        if(typeof value === 'string'){
        returnItem.push(<p>{key} : {value}</p>);
        }
        if(typeof value === 'array'){ //these are the engagements..
            console.log(value);
        }
        if(value.nodeType === 'document'){
             let document = value.content[0];
            docRender = documentToReactComponents(document); // -> <p>Hello world!</p>
        }
    }
    return (
       <div>
             {/* <h3>KOL:{kolDetails.fields.name}</h3> */}
             {returnItem}
             {docRender}
             <img src={value.imageUrl}></img>
       </div>
    )
}

export default (KolDetailsCard)