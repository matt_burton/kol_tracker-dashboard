import React, { useEffect } from "react";
import GenHook from "../../hooks/kol_hooks";
import { BrowserRouter as Router, Link } from "react-router-dom";
import {
  Button,
  Card,
  CardImg,
  CardTitle,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

const KolDeck = ({ kolDetails, allImages }) => {
  const { value, setStateValue } = GenHook(""); //Get Form Hook functions from input hook
  if (allImages[0]) {
    if (!value.imageUrl) {
      setStateValue("imageUrl", allImages[0].fields.file.url + "?w=150");
    }
  }

  let kolLink = "/details/" + kolDetails.sys.id;

  return (
    <Col md="4" className="cardDetails">
      <Row>
        <Col md="5" className="cardImg">
          <img width="100%" src={value.imageUrl} />
        </Col>
        <Col md="7">
          <h5>
            {kolDetails.fields.title} {kolDetails.fields.name}
          </h5>

          <Link to={kolLink} className="linkCard">
            View
          </Link>
        </Col>
      </Row>
    </Col>
  );
};

export default KolDeck;
