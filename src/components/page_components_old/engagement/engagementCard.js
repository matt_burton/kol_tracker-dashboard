import React from "react";
import { filter } from "lodash";

const EngagementCard = ({ engagement, drugs, cats }) => {
  let drugbyId;
  let catagory;
  let catagoryColor;
  filter(drugs, function(drug) {
    if (drug.sys.id === engagement.drug.sys.id) {
      drugbyId = drug.fields.drugtitle;
    }
  });

  filter(cats, function(cat) {
    if (cat.sys.id === engagement.caName.sys.id) {
      catagory = cat.fields.catName;
      catagoryColor = cat.fields.colour;
    }
  });

  return (
    <div style={{ "background-color": catagoryColor }}>
      <p>Engagement</p>
      <p>Date: {engagement.date}</p>
      <p>Location: {engagement.location}</p>
      <p>Type: {engagement.type}</p>
      <p>Drug: {drugbyId} </p>
      <p>Category: {catagory} </p>
    </div>
  );
};

export default EngagementCard;
