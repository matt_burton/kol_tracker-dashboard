import React from "react";

const DrugSelect = ({ drugs, setDrug }) => {
  return (
    <select onChange={setDrug}>
      <option value="reset">Please choose</option>
      {drugs.map((val, inx) => {
        return <option value={val.sys.id}>{val.fields.drugtitle}</option>;
      })}
    </select>
  );
};

export default DrugSelect;
