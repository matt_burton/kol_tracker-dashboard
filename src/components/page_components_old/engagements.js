import React, { Component } from "react";
import { connect } from "react-redux";
import { filterEngagementsYear } from "./engagement/functions";
import { sortBy, filter, uniq } from "lodash";
import EngagementsDisplay from "./engagement/engagementDisplay";
import DrugSelect from "./engagement/drugselect";

class EngagementsOver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      engagements: [],
      reset: false,
      allengagements: [],
      drugs: []
    };
    this.filterDate = this.filterDate.bind(this);
    this.filterDrug = this.filterDrug.bind(this);
  }

  componentDidMount() {
    let ordered = [];
    ordered = this.props.engagements;
    let orderedNf = [];
    filter(ordered, function(or) {
      // console.log(or.fields);
      orderedNf.push(or.fields);
    });

    let orderedFin = sortBy(orderedNf, function(dateObj) {
      return new Date(dateObj.date);
    });

    let reurnedUnique = filterEngagementsYear(orderedFin);
    this.setState({
      engagements: reurnedUnique,
      allengagements: reurnedUnique
    });
  }

  filterDate(dateVar) {
    // this.props.engagements = "";
    console.log(this.state.allengagements);
    let engagements;
    let chosenengagements;
    if (dateVar == "reset") {
      chosenengagements = this.state.allengagements;
    } else {
      engagements = this.state.engagements;
      chosenengagements = filter(engagements, function(ord) {
        if (ord.year === dateVar) {
          return ord;
        }
      });
    }
    this.setState({ engagements: chosenengagements, reset: true });
  }

  filterDrug(event) {
    let chosenengagements = [];
    if (event.target.value === "reset") {
      // console.log(this.state.allEngagements);
      chosenengagements = this.state.allengagements;
    } else {
      let engagements = this.state.allengagements;
      filter(engagements, function(ord) {
        let year = ord.year;
        let returnedengagements = [];
        let indrug = filter(ord.allEngagements, function(value) {
          // console.log(value.drug.sys.id);
          if (value.drug.sys.id === event.target.value) {
            returnedengagements.push(value);
          }
        });
        if (returnedengagements.length != 0) {
          chosenengagements.push({
            year: year,
            allEngagements: returnedengagements
          });
        }
      });
      console.log("chosen", chosenengagements);
    }
    this.setState({ engagements: chosenengagements, reset: true });
  }

  render() {
    let resetBut = "";
    if (this.state.reset) {
      resetBut = <p onClick={() => this.filterDate("reset")}>Reset</p>;
    }
    return (
      <div>
        {resetBut}
        <DrugSelect drugs={this.props.drugs} setDrug={this.filterDrug} />
        {this.state.engagements.map((val, inx) => {
          return (
            <div key={val.year}>
              <h3 onClick={() => this.filterDate(val.year)}>{val.year}</h3>
              <EngagementsDisplay
                engagement={val.allEngagements}
                drugs={this.props.drugs}
                cats={this.props.cats}
                reset={this.state.reset}
                setDate={this.filterDate}
              />
            </div>
          );
        })}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    kols: state.kols.all,
    images: state.kols.images,
    engagements: state.kols.engagements,
    drugs: state.kols.drugs,
    cats: state.kols.cats
  };
}

export default connect(mapStateToProps, {})(EngagementsOver);
