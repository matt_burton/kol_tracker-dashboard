import { sortBy, filter, uniq } from "lodash";
import React, { Component } from "react";

export const filterEngagementsYear = (engagements, dateValue) => {
  let year;
  let allyears = [];
  let allUnique = [];
  let returnedUnique = [];
  filter(engagements, function(value) {
    year = new Date(value.date);
    year = year.getFullYear();
    allyears.push(year);
  });
  allyears = uniq(allyears);
  //console.log(allyears);
  filter(allyears, function(value) {
    //for each year add the ordered
    allUnique = [];
    filter(engagements, function(engagement) {
      year = new Date(engagement.date);
      year = year.getFullYear();
      if (year === value) {
        allUnique.push(engagement);
      }
    });
    returnedUnique.push({ year: value, allEngagements: allUnique });
  });

  return returnedUnique;
};
