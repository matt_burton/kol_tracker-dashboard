import React from "react";

import EngagementCard from "./engagementCard";

const EngagementsDisplay = ({ engagement, drugs, cats, reset, setDate }) => {
  // let resetBut = "";
  // if (reset) {
  //   resetBut = <p onClick={() => setDate("reset")}>Reset</p>;
  // }

  return (
    <>
      {/* {resetBut} */}
      {engagement.map((val, inx) => {
        return <EngagementCard engagement={val} drugs={drugs} cats={cats} />;
      })}
    </>
  );
};

export default EngagementsDisplay;
