import React from "react";

const CatSelect = ({ cats, setCats }) => {
  return (
    <select onChange={setCats}>
      <option value="reset">Please choose</option>
      {cats.map((val, inx) => {
        return <option value={val.sys.id}>{val.fields.catName}</option>;
      })}
    </select>
  );
};

export default CatSelect;
