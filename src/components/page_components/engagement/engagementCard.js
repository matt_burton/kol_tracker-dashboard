import React, { useState } from "react";
import { filter } from "lodash";
import { BLOCKS, MARKS } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import {
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

const EngagementCard = ({ engagement, drugs, cats }) => {
  // cats.forEach(element => {
  //   if (element.sys.id === engagement.caName.sys.id) {
  //     console.log(element.fields.);
  //   }
  // });

  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const Bold = ({ children }) => <p className="bold">{children}</p>;

  const Text = ({ children }) => <p className="align-center">{children}</p>;

  const options = {
    renderMark: {
      [MARKS.BOLD]: text => <Bold>{text}</Bold>
    },
    renderNode: {
      [BLOCKS.PARAGRAPH]: (node, children) => <Text>{children}</Text>
    },
    renderText: text => text.replace("!", "?")
  };
  let drugbyId;
  let catagory;
  let catagoryColor;
  filter(drugs, function(drug) {
    if (drug.sys.id === engagement.drug.sys.id) {
      drugbyId = drug.fields.drugtitle;
    }
  });

  filter(cats, function(cat) {
    if (cat.sys.id === engagement.caName.sys.id) {
      catagory = cat.fields.catName;
      catagoryColor = cat.fields.colour;
    }
  });

  let returnItem = [];
  let docRender;
  for (let [key, value] of Object.entries(engagement)) {
    if (typeof value === "string") {
      returnItem.push(
        <p>
          <span class="fontTransition">{key}</span> : {value}
        </p>
      );
    }
    // if (typeof value === "array") {
    //   //these are the engagements..
    //   console.log(value);
    // }
    if (value.nodeType === "document") {
      let document = value.content[0];
      docRender = documentToReactComponents(document, options); // -> <p>Hello world!</p>
    }
  }

  return (
    <Col md="4" className="engagementCard">
      <span
        style={{ "background-color": catagoryColor }}
        className="spacer"
      ></span>
      <p className="titleBorder">{catagory}</p>
      <p>
        <span className="fontTransition">Date:</span> {engagement.date}
      </p>
      <p>
        <span className="fontTransition">Location:</span> {engagement.location}
      </p>

      <a onClick={toggle}>Details ></a>

      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>{catagory}</ModalHeader>
        <ModalBody>
          {returnItem}
          <span className="richText">{docRender}</span>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </Col>
  );
};

export default EngagementCard;
