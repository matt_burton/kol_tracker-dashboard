import React from "react";

const LinkSelect = ({ drugs, setDrug, nameString, typeString }) => {
  return (
    <select onChange={(evt) => setDrug(evt, nameString, typeString)}>
      <option value="reset">Please choose</option>
      {drugs.map((val, inx) => {
        return (
          <option nameRef={val.fields.title} value={val.sys.id}>
            {val.fields.title}
          </option>
        );
      })}
    </select>
  );
};

export default LinkSelect;
