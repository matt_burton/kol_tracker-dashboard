import React, { Component } from "react";
import { connect } from "react-redux";
import { filterEngagementsYear } from "./engagement/functions";
import { sortBy, filter, uniq } from "lodash";
import EngagementsDisplay from "./engagement/engagementDisplay";

import { Row, Col } from "reactstrap";

class EngagementsOver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      engagements: [],
      reset: false,
      allengagements: null,
      drugs: []
    };
  }

  componentDidMount() {}

  filterDate(dateVar) {
    // this.props.engagements = "";
    let engagements;
    let chosenengagements;
    if (dateVar == "reset") {
      chosenengagements = this.state.allengagements;
    } else {
      engagements = this.state.engagements;
      chosenengagements = filter(engagements, function(ord) {
        if (ord.year === dateVar) {
          return ord;
        }
      });
    }
    this.setState({ engagements: chosenengagements, reset: true });
  }

  filterDrug(event) {
    let chosenengagements = [];
    if (event.target.value === "reset") {
      // console.log(this.state.allEngagements);
      chosenengagements = this.state.allengagements;
    } else {
      let engagements = this.state.allengagements;
      filter(engagements, function(ord) {
        let year = ord.year;
        let returnedengagements = [];
        let indrug = filter(ord.allEngagements, function(value) {
          // console.log(value.drug.sys.id);
          if (value.drug.sys.id === event.target.value) {
            returnedengagements.push(value);
          }
        });
        if (returnedengagements.length != 0) {
          chosenengagements.push({
            year: year,
            allEngagements: returnedengagements
          });
        }
      });
    }
    this.setState({ engagements: chosenengagements, reset: true });
  }

  render() {
    let resetBut = "";
    if (this.state.reset) {
      resetBut = <p onClick={() => this.filterDate("reset")}>Reset</p>;
    }

    let ordered = [];
    ordered = this.props.engagementssingle;

    let orderedNf = [];
    filter(ordered, function(or) {
      // console.log(or.fields);
      orderedNf.push(or.fields);
    });

    let orderedFin = sortBy(ordered, function(dateObj) {
      return new Date(dateObj.date);
    });

    let reurnedUnique = filterEngagementsYear(orderedFin);
    if (!this.state.allengagements) {
      //this.setState({ allEngagements: reurnedUnique });
    }

    // if (this.state.engagement != reurnedUnique) {
    //   consolee("Set new state");
    // }
    // this.setState({
    //   engagements: reurnedUnique,
    //   allengagements: reurnedUnique
    // });
    // console.log("props", this.props);
    // console.log("kolsEngagements", kolsEngagements);

    return (
      <>
        {resetBut}
        {/* <DrugSelect drugs={this.props.drugs} setDrug={this.filterDrug} /> */}
        {reurnedUnique.map((val, inx) => {
          return (
            <>
              <Row key={val.year}>
                <Col>
                  <h3 onClick={() => this.filterDate(val.year)}>{val.year}</h3>
                </Col>
              </Row>
              <Row>
                <EngagementsDisplay
                  engagement={val.allEngagements}
                  drugs={this.props.drugs}
                  cats={this.props.cats}
                  reset={this.state.reset}
                  setDate={this.filterDate}
                />
              </Row>
            </>
          );
        })}
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    // kols: state.kols.all,
    // images: state.kols.images,
    engagements: state.kols.engagements
    // drugs: state.kols.drugs,
    // cats: state.kols.cats
  };
}

export default connect(mapStateToProps, {})(EngagementsOver);
