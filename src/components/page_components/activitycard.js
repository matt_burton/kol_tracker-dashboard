import React from "react";
import GenHook from "../../hooks/kol_hooks";
import { BrowserRouter as Router, Link } from "react-router-dom";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardText,
  CardTitle,
  CardSubtitle,
  Button,
} from "reactstrap";

const DetailCard = ({ details }) => {
  console.log("details", details);
  return (
    <Col md="4">
      <Row>
        <Card>
          <CardBody>
            <CardTitle>{details.fields.location}</CardTitle>
            <CardSubtitle>{details.fields.date}</CardSubtitle>
            <CardText>
              <p>
                <b>Type</b> {details.fields.type}
              </p>
            </CardText>
            <Button>View</Button>
          </CardBody>
        </Card>
      </Row>
    </Col>
  );
};

export default DetailCard;
