import React, { useEffect } from "react";
import GenHook from "../../hooks/kol_hooks";
import { BLOCKS, MARKS } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { Row, Col } from "reactstrap";

const KolDetailsCard = ({ kolDetails, allImages, kolsEngagements }) => {
  const { value, setStateValue } = GenHook(""); //Get Form Hook functions from input hook
  if (allImages[0]) {
    if (!value.imageUrl) {
      setStateValue("imageUrl", allImages[0].fields.file.url + "?w=250");
    }
  }

  const Bold = ({ children }) => <p className="bold">{children}</p>;

  const Text = ({ children }) => <p className="align-center">{children}</p>;

  const options = {
    renderMark: {
      [MARKS.BOLD]: text => <Bold>{text}</Bold>
    },
    renderNode: {
      [BLOCKS.PARAGRAPH]: (node, children) => <Text>{children}</Text>
    },
    renderText: text => text.replace("!", "?")
  };

  let returnItem = [];
  let docRender;
  for (let [key, value] of Object.entries(kolDetails.fields)) {
    if (typeof value === "string") {
      returnItem.push(
        <p>
          <span class="fontTransition">{key}</span> : {value}
        </p>
      );
    }
    // if (typeof value === "array") {
    //   //these are the engagements..
    //   console.log(value);
    // }
    if (value.nodeType === "document") {
      let document = value.content[0];
      docRender = documentToReactComponents(document, options); // -> <p>Hello world!</p>
    }
  }
  return (
    <div>
      <Row>
        {/* <h3>KOL:{kolDetails.fields.name}</h3> */}
        <Col md="4">
          <img src={value.imageUrl}></img>
        </Col>
        <Col md="8">
          {returnItem}
          {docRender}
        </Col>
      </Row>
    </div>
  );
};

export default KolDetailsCard;
