import React, { useEffect } from "react";
// import GenHook from '../../hooks/kol_hooks'
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { filter } from "lodash";

const KolEngagementCard = ({ kolsEngagements, drugs }) => {
  let returnItem = [];
  let docRender;
  for (let [key, value] of Object.entries(kolsEngagements)) {
    // console.log(key);
    if (key === "drug") {
      var result = filter(drugs, function(drug) {
        if (drug.sys.id === value.sys.id) {
          return drug;
        }
      });
      if (result[0]) {
        returnItem.push(<p>Drug : {result[0].fields.drugtitle};</p>);
      }
    }
    if (typeof value === "string") {
      returnItem.push(
        <p>
          {key} : {value}
        </p>
      );
    }
    if (value.nodeType === "document") {
      let document = value.content[0];
      docRender = documentToReactComponents(document); // -> <p>Hello world!</p>
    }
  }
  return (
    <div>
      {returnItem}
      {docRender}
    </div>
  );
};

export default KolEngagementCard;
