import React, { Component } from "react";
import { connect } from "react-redux";
import KolDetailsCard from "./page_components/koldetails";
import EngagementsOver from "./page_components/engagements";
import { filter } from "lodash";
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import Footer from "components/Footer/Footer.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import routes from "routes.js";
import { Route, Switch } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";
class DetailsKol extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: "blue",
      sidebarOpened:
        document.documentElement.className.indexOf("nav-open") !== -1
    };
  }
  componentWillMount() {}
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  populateData(val, id) {
    // console.log(val.fields.ebgagement);
    // console.log(this.props.engagements);
    let allEngagements = this.props.engagementsR;
    let kolsEngagements = [];

    filter(val.fields.ebgagement, function(engagment) {
      filter(allEngagements, function(propsEngagement) {
        if (propsEngagement.sys.id === engagment.sys.id) {
          kolsEngagements.push(propsEngagement.fields);
        }
      });
    });

    var imageProfile = filter(this.props.images, function(o) {
      if (o.sys.id == val.fields.profile.sys.id) return o;
    });

    return (

        <KolDetailsCard kolDetails={val} allImages={imageProfile} />
        <EngagementsOver
          engagementssingle={kolsEngagements}
          drugs={this.props.drugs}
          cats={this.props.cats}
        />

    );
  }
  render() {
    // console.log(this.props.drugs);
    return (
      <>
        <div className="wrapper">
          <Sidebar
            {...this.props}
            routes={routes}
            toggleSidebar={this.toggleSidebar}
          />

          <div className="main-panel" ref="mainPanel">
            <AdminNavbar
              {...this.props}
              brandText={this.getBrandText(this.props.location.pathname)}
              toggleSidebar={this.toggleSidebar}
              sidebarOpened={this.state.sidebarOpened}
            />
            <Switch>
              {this.getRoutes(routes)}
              <div className="content">
                <Row>
                  <h2>Users overview</h2>
                </Row>
                <Row>
                  <h2>All Kols</h2>
                  {this.props.kols.map((val, inx) => {
                    if (val.sys.id === this.props.match.params.id) {
                      return this.populateData(val, inx);
                    }
                  })}
                </Row>
              </div>
            </Switch>
            <Footer fluid />
          </div>
        </div>
      </>
      // <div>
      //   <h2>All Kols</h2>
      //   {this.props.kols.map((val, inx) => {
      //     if (val.sys.id === this.props.match.params.id) {
      //       return this.populateData(val, inx);
      //     }
      //   })}
      // </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    kols: state.kols.all,
    images: state.kols.images,
    engagements: state.kols.engagements,
    drugs: state.kols.drugs,
    cats: state.kols.cats
  };
}
export default connect(mapStateToProps, {})(DetailsKol);
