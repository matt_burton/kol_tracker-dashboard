import React, { Component } from "react";
import LinkSelect from "../page_components/engagement/linkselect";
import CKEditor from "ckeditor4-react";
import { updateTask } from "../../_services/update";
import { getStructure } from "../../_services/getStructure";
import { getbyId } from "../../_services/getbyId";

class UpdateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      structure: [],
      data: [],
      currentID: "",
    };
    this.filterLink = this.filterLink.bind(this);
    this.onEditorChange = this.onEditorChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.filterArray = this.filterArray.bind(this);
  }

  async componentDidUpdate() {
    if (this.props.CurrentId != "") {
      if (this.props.CurrentId != this.state.currentID) {
        let selectedRecords = await getbyId(
          this.props.Event,
          this.props.CurrentId
        );
        this.setState({
          data: selectedRecords,
          currentID: this.props.CurrentId,
        });
        let struct = await getStructure(this.props.Event);
        this.setState({
          structure: struct,
        });
        struct.map((val, inx) => {
          var d = new Date();
          let stringRet = "";
          // if (val.type === "Date") {
          //   stringRet = d;
          // }
          if (val.type === "Array") {
            stringRet = [];
            let newArray = [];
            let valueRet;
            if (this.state.data[val.id]) {
              valueRet = this.state.data[val.id]["en-US"];
            } else {
              valueRet = [];
            }
            this.setState({
              [val.name]: {
                value: valueRet,
                type: val.type,
                name: newArray,
              },
            });
          }
          if (val.type === "Link") {
            stringRet = [];
            let newArray = [];
            let id;
            if (this.state.data[val.id]["en-US"]) {
              id = this.state.data[val.id]["en-US"].sys.id;
            } else {
              id = "";
            }
            this.setState({
              [val.name]: {
                value: id,
                type: val.type,
                name: newArray,
              },
            });
          }

          if (
            val.type === "Symbol" ||
            val.type === "Text" ||
            val.type === "Date"
          ) {
            this.setState({
              [val.name]: {
                value: this.state.data[val.name.toLowerCase()]["en-US"],
                type: val.type,
              },
            });
          }
        });
      }
    }
  }
  async componentDidMount() {
    //currently saved to pass to create
  }

  handleChange = (e, type) => {
    let resultVar = e.target.name + "value";
    this.setState({
      [e.target.name]: { value: e.target.value, type: type },
    });
  };
  onEditorChange(evt, name, type) {
    this.setState({
      [name]: { value: evt.editor.getData(), type: type },
    });
  }

  filterLink(event, nameString, typeString) {
    this.setState({
      [nameString]: { value: event.target.value, type: typeString },
    });
  }

  filterArray(event, nameString, typeString) {
    var index = event.target.selectedIndex;
    var optionElement = event.target.childNodes[index];
    var option = optionElement.getAttribute("nameRef");

    //push to array of values
    let arrwithsys = {};
    arrwithsys.sys = {
      type: "Link",
      linkType: "Entry",
      id: event.target.value,
    };
    let tempARR = { ...this.state[nameString] };
    let tempA = tempARR.value.push(arrwithsys);
    let tempB = tempARR.name.push(option);
    this.setState({
      [nameString]: tempARR,
    });
  }

  submitForm(e) {
    e.preventDefault();
    updateTask(this.state, this.props.Event);
  }

  getMemberName(id) {
    let member = this.props.Members.find((member) => member.sys.id === id);
    return member.fields.title;
  }

  //get fields from type

  renderFormObject(value) {
    var d = new Date();
    if (value.type === "Date") {
      return (
        <label>
          {value.name}
          ><input name={value.name} value={d} />
        </label>
      );
    }
    if (value.type === "Symbol") {
      if (this.state[value.name]) {
        return (
          <label>
            {value.name}
            <input
              name={value.name}
              value={this.state[value.name].value}
              onChange={(e) => this.handleChange(e, value.type)}
            />
          </label>
        );
      }
    }
    if (value.type === "Array") {
      if (this.state[value.name]) {
        return (
          <label>
            {value.name}
            <LinkSelect
              drugs={this.props[value.name]}
              setDrug={this.filterArray}
              typeString={value.type}
              nameString={value.name}
            />
            {this.state[value.name].value.map((val, inx) => {
              let memberName = this.getMemberName(val.sys.id);
              return (
                <>
                  <p>Name: {memberName}</p>
                </>
              );
            })}
          </label>
        );
      }
    }
    if (value.type === "Text") {
      if (this.state[value.name]) {
        return (
          <CKEditor
            data={this.state[value.name].value}
            onChange={(evt) => this.onEditorChange(evt, value.name, value.type)}
          />
        );
      }
    }
    if (value.type === "Link") {
      if (this.state[value.name]) {
        return (
          <>
            <label>
              {value.name}
              <input
                type="hidden"
                name="chosenDrug"
                value={this.state[value.name].value}
              />
              <LinkSelect
                drugs={this.props[value.name]}
                setDrug={this.filterLink}
                typeString={value.type}
                nameString={value.name}
              />
            </label>
          </>
        );
      }
    }
  }

  render() {
    //build up  forms elements
    return (
      <>
        {/* {this.props.CurrentId} */}
        <form>
          {this.state.structure.map((val, inx) => {
            return this.renderFormObject(val);
          })}
          <button onClick={this.submitForm}>Submit</button>
        </form>
      </>
    );
  }
}

export default UpdateForm;
