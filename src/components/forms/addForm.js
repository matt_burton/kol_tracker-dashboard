import React, { Component } from "react";
import LinkSelect from "../page_components/engagement/linkselect";
import CKEditor from "ckeditor4-react";
import { postTask } from "../../_services/create";
import { getStructure } from "../../_services/getStructure";

class AddForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      structure: []
    };
    this.filterLink = this.filterLink.bind(this);
    this.onEditorChange = this.onEditorChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.filterArray = this.filterArray.bind(this);
  }
  async componentDidMount() {
    let struct = await getStructure(this.props.Event);
    //currently saved to pass to create
    this.setState({
      structure: struct
    });
    struct.map((val, inx) => {
      var d = new Date();
      let stringRet = "";
      if (val.type === "Date") {
        stringRet = d;
      }
      if (val.type === "Array") {
        stringRet = [];
        let newArray = [];
        this.setState({
          [val.name]: { value: stringRet, type: val.type, name: newArray }
        });
      } else {
        this.setState({
          [val.name]: { value: stringRet, type: val.type }
        });
      }
    });
  }

  handleChange = (e, type) => {
    let resultVar = e.target.name + "value";
    this.setState({
      [e.target.name]: { value: e.target.value, type: type }
    });
  };
  onEditorChange(evt, name, type) {
    this.setState({
      [name]: { value: evt.editor.getData(), type: type }
    });
  }

  filterLink(event, nameString, typeString) {
    this.setState({
      [nameString]: { value: event.target.value, type: typeString }
    });
  }

  filterArray(event, nameString, typeString) {
    var index = event.target.selectedIndex;
    var optionElement = event.target.childNodes[index];
    var option = optionElement.getAttribute("nameRef");

    //push to array of values
    let tempARR = { ...this.state[nameString] };
    let tempA = tempARR.value.push(event.target.value);
    let tempB = tempARR.name.push(option);
    console.log("state", this.state[nameString]);
    this.setState({
      [nameString]: tempARR
    });
  }

  submitForm(e) {
    e.preventDefault();
    postTask(this.state, this.props.Event);
  }

  //get fields from type

  renderFormObject(value) {
    var d = new Date();
    if (value.type === "Date") {
      return (
        <label>
          {value.name}
          ><input name={value.name} value={d} />
        </label>
      );
    }
    if (value.type === "Symbol") {
      if (this.state[value.name]) {
        return (
          <label>
            {value.name}
            <input
              name={value.name}
              value={this.state[value.name].value}
              onChange={e => this.handleChange(e, value.type)}
            />
          </label>
        );
      }
    }
    if (value.type === "Array") {
      if (this.state[value.name]) {
        return (
          <label>
            {value.name}
            <LinkSelect
              drugs={this.props[value.name]}
              setDrug={this.filterArray}
              typeString={value.type}
              nameString={value.name}
            />
            {this.state[value.name].value.map((val, inx) => {
              return (
                <>
                  <p>Name: {this.state[value.name].name[inx]}</p>
                </>
              );
            })}
          </label>
        );
      }
    }
    if (value.type === "Text") {
      if (this.state[value.name]) {
        return (
          <CKEditor
            data={this.state[value.name].value}
            onChange={evt => this.onEditorChange(evt, value.name, value.type)}
          />
        );
      }
    }
    if (value.type === "Link") {
      if (this.state[value.name]) {
        return (
          <>
            <label>
              {value.name}
              <input
                type="text"
                name="chosenDrug"
                value={this.state[value.name].value}
              />
              <LinkSelect
                drugs={this.props[value.name]}
                setDrug={this.filterLink}
                typeString={value.type}
                nameString={value.name}
              />
            </label>
          </>
        );
      }
    }
  }

  render() {
    //build up  forms elements
    return (
      <form>
        {this.state.structure.map((val, inx) => {
          return this.renderFormObject(val);
        })}
        <button onClick={this.submitForm}>Submit</button>
      </form>
    );
  }
}

export default AddForm;
