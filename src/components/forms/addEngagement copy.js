import React, { useEffect } from "react";
import DrugSelect from "../page_components/engagement/drugselect";
import CatSelect from "../page_components/engagement/catsselect";
// import GenHook from "../../hooks/kol_hooks";
// import { BrowserRouter as Router, Link } from "react-router-dom";
import { Row, Col } from "reactstrap";

const EngagementForm = ({
  drugs,
  filterDrug,
  chosenDrug,
  cats,
  filterCats,
  chosenCat
}) => {
  //   const { value, setStateValue } = GenHook(""); //Get Form Hook functions from input hook
  //   if (allImages[0]) {
  //     if (!value.imageUrl) {
  //       setStateValue("imageUrl", allImages[0].fields.file.url + "?w=150");
  //     }
  //   }

  //   let kolLink = "/details/" + kolDetails.sys.id;
  console.log(chosenDrug);
  return (
    <Col md="4" className="cardDetails">
      <Row>
        <form>
          <label>
            Name:
            <input type="text" name="name" />
          </label>

          <input type="text" name="chosenDrug" value={chosenDrug} />
          <input type="text" name="chosenCat" value={chosenCat} />
          <DrugSelect drugs={drugs} setDrug={filterDrug} />
          <CatSelect cats={cats} setCats={filterCats} />
          <input type="submit" value="Submit" />
        </form>
      </Row>
    </Col>
  );
};

export default EngagementForm;
