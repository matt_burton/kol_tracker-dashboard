import axios from "axios";
import { apiData } from "../config";
export const FETCH_KOLS = "FETCH_KOLS";
export const FETCH_TITLES = "FETCH_TITLES";
export const FETCH_PROFILE = "FETCH_PROFILE";
export const FETCH_ENGAGEMENT = "FETCH_ENGAGEMENT";
export const FETCH_DRUGS = "FETCH_DRUGS";
export const FETCH_CATAGORIES = "FETCH_CATAGORIES";

const API_BASE_URL = apiData.API_BASE_URL;
const API_SPACE_ID = apiData.API_SPACE_ID;
const API_TOKEN = apiData.API_TOKEN;

export function fetchKols() {
  const request = axios.get(
    `${API_BASE_URL}/spaces/${API_SPACE_ID}/entries?access_token=${API_TOKEN}&content_type=kolDetails`
  );
  return {
    type: FETCH_KOLS,
    payload: request,
  };
}

export function fetchTitles() {
  const request = axios.get(
    `${API_BASE_URL}/spaces/${API_SPACE_ID}/entries?access_token=${API_TOKEN}&content_type=title`
  );
  return {
    type: FETCH_TITLES,
    payload: request,
  };
}

export function fetchCats() {
  const request = axios.get(
    `${API_BASE_URL}/spaces/${API_SPACE_ID}/entries?access_token=${API_TOKEN}&content_type=engagementCategory`
  );
  return {
    type: FETCH_CATAGORIES,
    payload: request,
  };
}

export function fetchEngagement() {
  const request = axios.get(
    `${API_BASE_URL}/spaces/${API_SPACE_ID}/entries?access_token=${API_TOKEN}&content_type=engagement`
  );
  return {
    type: FETCH_ENGAGEMENT,
    payload: request,
  };
}

export function fetchDrugs() {
  const request = axios.get(
    `${API_BASE_URL}/spaces/${API_SPACE_ID}/entries?access_token=${API_TOKEN}&content_type=drugs`
  );
  return {
    type: FETCH_DRUGS,
    payload: request,
  };
}

export function fetchProfileImg(id) {
  const request = axios.get(
    `${API_BASE_URL}/spaces/${API_SPACE_ID}/environments/master/assets?access_token=${API_TOKEN}`
  );
  return {
    type: FETCH_PROFILE,
    payload: request,
  };
}
