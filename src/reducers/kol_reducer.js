import {
  FETCH_KOLS,
  FETCH_PROFILE,
  FETCH_ENGAGEMENT,
  FETCH_DRUGS,
  FETCH_TITLES,
  FETCH_CATAGORIES
} from "../actions/index";

const INITIAL_STATE = {
  all: [],
  titles: [],
  images: [],
  engagements: [],
  drugs: [],
  cats: []
};
export default function(state = INITIAL_STATE, action) {
  // console.log(action.type, action.payload);
  switch (action.type) {
    case FETCH_CATAGORIES:
      return { ...state, cats: action.payload.data.items };
    case FETCH_KOLS:
      return { ...state, all: action.payload.data.items };
    case FETCH_ENGAGEMENT:
      return { ...state, engagements: action.payload.data.items };

    case FETCH_DRUGS:
      return { ...state, drugs: action.payload.data.items };

    case FETCH_PROFILE:
      return { ...state, images: action.payload.data.items };
    // return state;
    case FETCH_TITLES:
      return { ...state, titles: action.payload.data.items };
    // return state;
    default:
      return state;
  }
}
