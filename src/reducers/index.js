import { combineReducers } from 'redux';

import KolReducer from './kol_reducer';
const rootReducer = combineReducers({
  kols: KolReducer
});

export default rootReducer;
