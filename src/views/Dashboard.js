import React, { Component } from "react";
import { connect } from "react-redux";

import { Route, Switch, Redirect } from "react-router-dom";
import routes from "routes.js";

import AddForm from "../components/forms/addForm";
import UpdateForm from "../components/forms/updateForm";
// core components
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import Footer from "components/Footer/Footer.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import DetailCard from "components/page_components/activitycard";

import {
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { any } from "prop-types";
class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: "blue",
      modalcontent: "engagement",
      modaltoggle: false,
      modaltoggleUpdate: false,
      chosenId: "",
      sidebarOpened:
        document.documentElement.className.indexOf("nav-open") !== -1,
    };
  }
  componentWillMount() {}
  setUpdate(dataId) {
    this.setState({
      chosenId: dataId,
      modaltoggleUpdate: true,
    });
  }
  getRoutes = (routes) => {
    return routes.map((prop, key) => {
      if (prop.layout === "/dashboard") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  toggle(modal) {
    this.setState({
      modaltoggle: !this.state.modaltoggle,
      modal: modal,
    });
  }

  render() {
    // console.log(this.props);
    return (
      <>
        <div className="wrapper">
          <Sidebar
            {...this.props}
            routes={routes}
            // logo={{
            //   outterLink: "https://www.creative-tim.com/",
            //   text: "Creative Tim",
            //   imgSrc: logo
            // }}
            toggleSidebar={this.toggleSidebar}
          />
          <div className="main-panel" ref="mainPanel">
            <AdminNavbar
              {...this.props}
              toggleSidebar={this.toggleSidebar}
              sidebarOpened={this.state.sidebarOpened}
            />
            <Switch>
              {this.getRoutes(routes)}
              <div className="content">
                <Row>
                  <h2>Dashboard</h2>
                  <Button
                    color="secondary"
                    onClick={() => {
                      this.toggle("engagement");
                    }}
                  >
                    Add Activity
                  </Button>
                  <Button
                    color="secondary"
                    onClick={() => {
                      this.toggle("engagementCategory");
                    }}
                  >
                    Add Category
                  </Button>
                </Row>
                <Row>
                  {this.props.engagements.map((val, inx) => {
                    return <DetailCard details={val} />;
                    // return (
                    //   <a
                    //     onClick={() => {
                    //       this.setUpdate(val.sys.id);
                    //     }}
                    //   >
                    //     {val.fields.location}
                    //   </a>
                    // );
                  })}
                </Row>

                <Modal isOpen={this.state.modaltoggleUpdate}>
                  <ModalBody>
                    <UpdateForm
                      Drug={this.props.drugs}
                      Category={this.props.cats}
                      Members={this.props.kols}
                      Event="engagement"
                      CurrentId={this.state.chosenId}
                    />
                  </ModalBody>
                  <ModalFooter>
                    <Button
                      color="secondary"
                      onClick={() => {
                        this.toggle(this.state.modal);
                      }}
                    >
                      Cancel
                    </Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modaltoggle}>
                  <ModalBody>
                    <AddForm
                      Drug={this.props.drugs}
                      Category={this.props.cats}
                      Members={this.props.kols}
                      Event={this.state.modal}
                    />
                  </ModalBody>
                  <ModalFooter>
                    <Button
                      color="secondary"
                      onClick={() => {
                        this.toggle(this.state.modal);
                      }}
                    >
                      Cancel
                    </Button>
                  </ModalFooter>
                </Modal>
              </div>
            </Switch>
            {
              // we don't want the Footer to be rendered on map page
              this.props.location.pathname.indexOf("maps") !== -1 ? null : (
                <Footer fluid />
              )
            }
          </div>
        </div>
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    kols: state.kols.all,
    images: state.kols.images,
    engagements: state.kols.engagements,
    drugs: state.kols.drugs,
    cats: state.kols.cats,
  };
}
export default connect(mapStateToProps, {})(Dashboard);
