import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import PrivateRoute from "./components/PrivateRoute";

import {
  fetchKols,
  fetchProfileImg,
  fetchEngagement,
  fetchDrugs,
  fetchCats,
} from "actions/index";

import Posts from "./components/posts";
import DetailsKol from "./components/details";
import Dashboard from "./views/Dashboard.js";
import { useAuth0 } from "./react-auth0-spa";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    this.props.fetchKols();
    this.props.fetchProfileImg();
    this.props.fetchEngagement();
    this.props.fetchDrugs();
    this.props.fetchCats();
  }

  render() {
    return (
      <BrowserRouter>
        <div className="appWrapper">
          <Route exact path="/" component={Posts} />
          {/* <Route exact path="/dashboard" component={Dashboard} /> */}
          <Route
            exact
            path="/dashboard"
            render={(props) => <Dashboard {...props} />}
          />
          <Route
            exact
            path="/details/:id"
            render={(props) => <DetailsKol {...props} />}
          />
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
  fetchKols,
  fetchProfileImg,
  fetchEngagement,
  fetchDrugs,
  fetchCats,
})(App);
