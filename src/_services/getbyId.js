import { apiData } from "../config";
import { createClient } from "contentful-management";

var client = createClient({
  accessToken: apiData.MANAGEMENT_TOKEN,
});

export const getbyId = (data, id) => {
  //get content type
  let structureData = client
    .getSpace(apiData.API_SPACE_ID)
    .then((space) => space.getEntry(id))
    .then((entry) => {
      console.log("entry", entry.fields);
      return entry.fields;
    })
    .catch(console.error);

  return structureData;
};
// /spaces/019adgjkmrxy / environments / staging / entries;
