import { apiData } from "../config";
import { createClient } from "contentful-management";
import {
  fetchKols,
  fetchProfileImg,
  fetchEngagement,
  fetchDrugs,
  fetchCats,
} from "actions/index";

var client = createClient({
  accessToken: apiData.MANAGEMENT_TOKEN,
});

export const updateTask = (data, event) => {
  let returnFields = { fields: {} };
  //Create fields object from data.
  data.structure.map((val, inx) => {
    let fieldData = data[val.name];
    if (fieldData) {
      if (
        fieldData.type === "Symbol" ||
        fieldData.type === "Text" ||
        fieldData.type === "Date"
      ) {
        returnFields.fields[val.id] = { "en-US": fieldData.value };
        data.data[val.id] = { "en-US": fieldData.value };
      }
      if (fieldData.type === "Link") {
        returnFields.fields[val.id] = {
          "en-US": {
            sys: { type: "Link", linkType: "Entry", id: fieldData.value },
          },
        };
        data.data[val.id] = {
          "en-US": {
            sys: { type: "Link", linkType: "Entry", id: fieldData.value },
          },
        };
      }
      if (fieldData.type === "Array") {
        let arrFields = [];
        fieldData.value.map((value, index) => {
          arrFields.push({
            sys: {
              type: "Link",
              linkType: "Entry",
              id: value.sys.id,
            },
          });
        });
        returnFields.fields[val.id] = {
          "en-US": arrFields,
        };
        data.data[val.id] = { "en-US": arrFields };
      }
    }
  });
  //loop around structure and build fields
  client.getSpace("pt91c6qdok7e").then((space) => {
    // Now that we have a space, we can get entries from that space
    space.getEntry(data.currentID).then((entry) => {
      entry.fields = returnFields.fields;
      //console.log(entry.fields.members, returnFields.fields.members);
      return entry.update().then((response) => {
        space.getEntry(response.sys.id).then((entry) => entry.publish());
        window.location.reload(false);
      });
    });
  });
};
