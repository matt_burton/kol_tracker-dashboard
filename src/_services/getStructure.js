import { apiData } from "../config";
import { createClient } from "contentful-management";

var client = createClient({
  accessToken: apiData.MANAGEMENT_TOKEN,
});

export const getStructure = (data) => {
  //get content type
  let structureData = client
    .getSpace(apiData.API_SPACE_ID)
    .then((space) => space.getContentType(data))
    .then((content) => {
      return content.fields;
    })
    .catch(console.error);

  return structureData;
};
// /spaces/019adgjkmrxy / environments / staging / entries;
